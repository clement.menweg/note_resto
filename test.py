"""
This module contains unit tests for the Flask application defined in note.py.
"""
import unittest
from note import app


class TestAppRoutes(unittest.TestCase):
    """
    This class contains unit tests for the routes in the Flask application.
    """
    def setUp(self):
        """
        Sets up the test client before each test.
        """
        print("Setting up the test client...")
        app.config["TESTING"] = True
        self.app = app.test_client()

    def test_index(self):
        """
        Tests the GET / route.
        """
        print("Testing GET /...")
        response = self.app.get("/")
        print(f"Status code: {response.status_code}")
        self.assertEqual(response.status_code, 200)

    def test_note_resto(self):
        """
        Tests the GET /note_resto route.
        """
        print("Testing GET /note_resto...")
        response = self.app.get("/note_resto")
        print(f"Status code: {response.status_code}")
        self.assertEqual(response.status_code, 200)

    def test_note_resto_post(self):
        """
        Tests the POST /note_resto route.
        """
        print("Testing POST /note_resto...")
        response = self.app.post(
            "/note_resto",
            data={
                "username": "test", 
                "family_member": "test", 
                "expenses": "100", 
                "family_costs": "{}"
            },
            follow_redirects=True,
        )
        print(f"Status code: {response.status_code}")
        self.assertEqual(response.status_code, 200)

    def tearDown(self):
        print("Tearing down after tests...")


if __name__ == "__main__":
    unittest.main()
