"""
This module defines a Flask application for managing expenses.
"""
import re
from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)

users = {}
common_expenses = {}
family_costs_total = {}


@app.route("/", methods=["GET", "POST"])
def achat():
    """
    Handles the main route. If the request method is POST, it processes the form data
    and calculates the shared cost of an expense. If the request method is GET, it
    renders the main page.
    """
    resultat = None
    if request.method == "POST":
        montant = float(request.form.get("montant").replace(",", "."))
        participants = int(request.form.get("participants"))

        # Vérifier que le montant est supérieur à 0

        if montant <= 0:
            return "Erreur : Le montant total de l'achat doit être supérieur à 0.", 400
        # Vérifier que le nombre de participants est un entier positif

        if participants <= 0 or not isinstance(participants, int):
            return (
                "Erreur : Le nombre de participants doit être un entier supérieur à 0.",
                400,
            )
        resultat = montant / participants
    return render_template("achat.j2", resultat=resultat)


@app.route("/note_resto", methods=["GET", "POST"])
def note_restaurant():
    """
    Handles the '/note_resto' route. If the request method is POST, it processes the form data
    and calculates the family expenses. If the request method is GET, it renders the page.
    """
    if request.method == "POST":
        username = request.form.get("username")
        family_member = request.form.get("family_member")
        expenses = request.form.get("expenses")
        #family_costs = request.args.get("family_costs", "{}")
        family_expenses = 0.0

        if expenses:
            expenses = float(expenses.replace(",", "."))
        else:
            expenses = 0.0
        if username and re.match("^[A-Za-zÀ-ÖØ-öø-ÿ]{1,15}$", username):
            if username not in users:
                users[username] = {
                    "expenses": 0,
                    "family": [],
                    "total": 0,
                    "common_expenses": {},
                }
            if family_member and re.match("^[A-Za-zÀ-ÖØ-öø-ÿ]{1,15}$", family_member):
                family_expenses = expenses
                users[username]["family"].append(
                    {
                        "name": family_member,
                        "expenses": family_expenses,
                        "common_expenses": {},
                    }
                )
                users[username]["total"] += family_expenses
            if expenses and expenses >= 0:
                users[username]["expenses"] += expenses - family_expenses
                users[username]["total"] += expenses - family_expenses
        else:
            return "Input invalide", 400
        return redirect(url_for("note_restaurant"))
    return render_template(
        "note_restaurant.j2",
        users=users,
        common_expenses=common_expenses,
        family_costs_total=family_costs_total,
    )


@app.route("/add_common_expense", methods=["POST"])
def add_common_expense():
    """
    Handles the '/add_common_expense' route. If the request method is POST,
    it processes the form data and adds a common expense to the list. 
    """
    item_name = request.form.get("item_name")
    item_price = request.form.get("item_price")
    if item_price:  # Check if item_price is not an empty string
        item_price = float(item_price.replace(",", "."))
    else:
        item_price = 0.0  # Default to 0.0 if item_price is an empty string
    common_expenses[item_name] = item_price
    return redirect(url_for("note_restaurant"))


@app.route("/divide_common_expense", methods=["POST"])
def divide_common_expense():
    """
    Handles the '/divide_common_expense' route. If the request method is POST,
    it processes the form data and divides a common expense among the users.
    """
    item_name = request.form.get("item_name")
    users_list = request.form.getlist("users")
    item_price_per_person = round(common_expenses[item_name] / len(users_list), 2)
    family_costs = {}

    for username in users_list:
        if username in users:
            users[username]["common_expenses"][item_name] = float(item_price_per_person)
            family_name = users[username].get("family_name")
            if family_name not in family_costs:
                family_costs[family_name] = float(item_price_per_person)
            else:
                family_costs[family_name] += float(item_price_per_person)
        else:
            for user in users.values():
                family_member_count = len(user["family"])
                for member in user["family"]:
                    if member["name"] == username:
                        member["common_expenses"][item_name] = (
                            float(item_price_per_person) * family_member_count
                        )
                        family_name = member.get("family_name")
                        if family_name not in family_costs:
                            family_costs[family_name] = (
                                float(item_price_per_person) * family_member_count
                            )
                        else:
                            family_costs[family_name] += (
                                float(item_price_per_person) * family_member_count
                            )
    # Update the total for each user

    for user in users.values():
        user["total"] = user["expenses"]
        for member in user["family"]:
            member["total"] = member["expenses"] + sum(
                member["common_expenses"].values()
            )
            user["total"] += member[
                "total"
            ]  # Add the total of each family member to the user's total
        user["total"] += sum(
            user["common_expenses"].values()
        )  # Add the user's common expenses to their total
    print(users)
    del common_expenses[item_name]

    return redirect(url_for("note_restaurant"))


@app.route("/reset", methods=["POST"])
def reset_data():
    """
    Handles the '/reset' route. If the request method is POST, it resets the
    data by clearing the users and common_expenses.
    """
    users.clear()
    common_expenses.clear()
    return redirect(url_for("note_restaurant"))


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
