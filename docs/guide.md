# Manuel de l'utilisateur

## Exécution locale de l'application

Pour exécuter l'application localement, naviguez jusqu'au répertoire du projet et exécutez la commande suivante :

```bash
python app.py
```
## Exécution des tests
Pour exécuter les tests, utilisez la commande suivante :
```bash
python test.py
```

Construction de l'image Docker
Pour construire une image Docker pour l'application, utilisez la commande suivante :
```bash
docker build -t my-app:latest .
```

Ceci construira une image Docker avec le tag my-app:latest.

## Utilisation de l'image Docker
Pour exécuter l'application à l'aide de l'image Docker, utilisez la commande suivante :
```bash
docker run -p 5000:5000 my-app:latest
```

Cela démarrera l'application et la rendra accessible à l'adresse http://localhost:5000.

## Paramètres du pipeline
Le pipeline est configuré à l'aide du fichier .gitlab-ci.yml. Il comporte trois étapes : validate, test, build et pages.

L'étape de test exécute les tests unitaires.
L'étape de construction construit l'image Docker.
La phase de déploiement déploie l'application.
Vous pouvez modifier le fichier .gitlab-ci.yml pour changer la configuration du pipeline. Par exemple, vous pouvez ajouter de nouvelles étapes, changer l'image Docker utilisée, ou modifier les commandes exécutées dans chaque étape.

N'oubliez pas de valider et de pousser les modifications apportées au fichier .gitlab-ci.yml pour les appliquer au pipeline.