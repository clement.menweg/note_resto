# Développement de l'application de gestion des dépenses

Ce document suit le développement en cours de l'application de gestion des dépenses basée sur Flask.

## Fonctionnalités actuelles

### Gestion des achats
- La route '/' gère les achats en calculant le coût partagé d'une dépense.
- Vérification des montants saisis pour s'assurer qu'ils sont supérieurs à zéro.
- Division équitable des dépenses en fonction du nombre de participants.

### Gestion des notes de restaurant
- La route '/note_resto' gère les dépenses familiales dans les restaurants.
- Enregistrement et totalisation des dépenses par membre de la famille.
- Possibilité d'ajouter et de partager les dépenses communes entre les utilisateurs.

### Fonctionnalités supplémentaires
- Ajout et partage de dépenses communes entre utilisateurs.

## Détails techniques

### Gestion des données utilisateur
- Utilisation de structures de données telles que des dictionnaires pour stocker les informations des utilisateurs, leurs dépenses et les dépenses communes.
- Validation des entrées utilisateurs pour garantir des données précises et cohérentes.

### Routage et contrôle des vues
- Utilisation de routes Flask pour gérer différents aspects de l'application, comme la saisie des dépenses et l'affichage des résultats.
- Contrôle des méthodes HTTP (GET, POST) pour manipuler les données transmises par les utilisateurs.

### Logique métier
- Calculs précis des dépenses partagées et individuelles.
- Gestion des dépenses familiales enregistrées par utilisateur.

---

Ce document continuera à être mis à jour pour refléter l'évolution du développement de l'application de gestion des dépenses.
