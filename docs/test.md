# Tests unitaires pour l'application Flask

Ce document décrit les tests unitaires pour l'application Flask définie dans `note.py`.

## Description

Les tests sont conçus pour vérifier le fonctionnement correct des routes de l'application Flask.

## Structure des tests

### `TestAppRoutes` (Classe de tests)

Cette classe contient des tests pour les différentes routes de l'application Flask.

#### Méthodes de test

##### `setUp()`
- Méthode pour configurer le client de test avant chaque test.
- Met l'application en mode "TESTING".

##### `test_index()`
- Vérifie le fonctionnement de la route GET `/`.
- Effectue une requête GET et vérifie que le code de statut de la réponse est 200 (OK).

##### `test_note_resto()`
- Vérifie le fonctionnement de la route GET `/note_resto`.
- Effectue une requête GET et vérifie que le code de statut de la réponse est 200 (OK).

##### `test_note_resto_post()`
- Vérifie le fonctionnement de la route POST `/note_resto`.
- Envoie une requête POST avec des données simulées et vérifie que le code de statut de la réponse est 200 (OK).

##### `tearDown()`
- Méthode pour nettoyer après chaque test.

## Exécution des tests

Pour exécuter les tests, utilisez la commande suivante :

```bash
python test.py
```